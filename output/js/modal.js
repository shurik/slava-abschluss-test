document.addEventListener('DOMContentLoaded', function() {
  let modal;
  const modalOverlay = document.getElementById('modal-overlay');

  const closeModal = () => {
    modal.classList.remove('show');
    modalOverlay.classList.remove('show');
  }

  modalOverlay.addEventListener('click', closeModal);

  document.querySelectorAll('.modal').forEach((n) => {
    n.addEventListener('click', (e) => {
      closeModal();
      e.stopPropagation();
    });
  });

  document.querySelectorAll('.month').forEach((n) => {
    n.addEventListener('click', (e) => {
      modal = e.target.getElementsByTagName('div')[0];
      modal.classList.add('show');
      modalOverlay.classList.add('show');
    });
  });
});
