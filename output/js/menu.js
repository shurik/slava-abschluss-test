document.addEventListener('DOMContentLoaded', function() {
  const nav = document.querySelector('nav');

  nav.classList.add('close');
  nav.onclick = function(e) {
    if (e.target.parentNode.id === 'burger') {
      nav.classList.toggle('close');
    } else {
      nav.classList.add('close');
    }
  };
});
